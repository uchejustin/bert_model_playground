""" Module for creating bert model with pytorch """

from typing import Tuple, List
from functools import partial

import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader, RandomSampler
from torch.nn.utils.rnn import pad_sequence
from transformers import BertTokenizer, BertModel, AdamW, get_linear_schedule_with_warmup, BertPreTrainedModel
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from tqdm import tqdm


PATH_TO_DATA = "/content/drive/My Drive/other_ml_data"
BERT_MODEL_NAME = 'bert-base-cased'
DEVICE = torch.device('cpu')
if torch.cuda.is_available():
    DEVICE = torch.device('cuda:0')
TOKENIZER = BertTokenizer.from_pretrained(BERT_MODEL_NAME)
LABEL_COLS = ['toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']
EPOCH_NUM = 2
BATCH_SIZE = 32


def setup_training_data(path_to_data_files=PATH_TO_DATA):
    df_train = pd.read_csv(f'{path_to_data_files}/train.csv', sep=',')
    train_df, val_df = train_test_split(df_train, test_size=0.05)
    return train_df, val_df


def setup_test_data(path_to_data_files=PATH_TO_DATA):
    df_test = pd.read_csv(f'{path_to_data_files}/test.csv')
    df_test_labels = pd.read_csv(f'{path_to_data_files}/test_labels.csv')
    df_submission = pd.read_csv(f'{path_to_data_files}/sample_submission.csv')
    return df_test, df_test_labels, df_submission


class ToxicDataset(Dataset):
    
    def __init__(self, TOKENIZER: BertTokenizer, dataframe: pd.DataFrame, lazy: bool = False):
        self.tokenizer = TOKENIZER
        self.pad_idx = TOKENIZER.pad_token_id
        self.lazy = lazy
        if not self.lazy:
            self.X = []
            self.Y = []
            for i, (row) in tqdm(dataframe.iterrows()):
                x, y = self.row_to_tensor(self.tokenizer, row)
                self.X.append(x)
                self.Y.append(y)
        else:
            self.df = dataframe        
    
    @staticmethod
    def row_to_tensor(TOKENIZER: BertTokenizer, row: pd.Series) -> Tuple[torch.LongTensor, torch.LongTensor]:
        tokens = TOKENIZER.encode(row["comment_text"], add_special_tokens=True)
        if len(tokens) > 120:
            tokens = tokens[:119] + [tokens[-1]]
        x = torch.LongTensor(tokens)
        y = torch.FloatTensor(row[LABEL_COLS])
        return x, y
        
    
    def __len__(self):
        if self.lazy:
            return len(self.df)
        else:
            return len(self.X)

    def __getitem__(self, index: int) -> Tuple[torch.LongTensor, torch.LongTensor]:
        if not self.lazy:
            return self.X[index], self.Y[index]
        else:
            return self.row_to_tensor(self.tokenizer, self.df.iloc[index])
            

def collate_fn(batch: List[Tuple[torch.LongTensor, torch.LongTensor]], device: torch.device) \
        -> Tuple[torch.LongTensor, torch.LongTensor]:
    x, y = list(zip(*batch))
    x = pad_sequence(x, batch_first=True, padding_value=0)
    y = torch.stack(y)
    return x.to(device), y.to(device)



def setup_pytorch_dataloader(train_df: pd.DataFrame, val_df: pd.DataFrame, data_extractor=collate_fn):
    train_dataset = ToxicDataset(TOKENIZER, train_df, lazy=True)
    dev_dataset = ToxicDataset(TOKENIZER, val_df, lazy=True)
    collate_fn = partial(data_extractor, device=DEVICE)
    train_sampler = RandomSampler(train_dataset)
    dev_sampler = RandomSampler(dev_dataset)
    train_iterator = DataLoader(train_dataset, batch_size=BATCH_SIZE, sampler=train_sampler, collate_fn=collate_fn)
    dev_iterator = DataLoader(dev_dataset, batch_size=BATCH_SIZE, sampler=dev_sampler, collate_fn=collate_fn)
    return dev_iterator, train_iterator


class BertClassifier(nn.Module):
    
    def __init__(self, bert: BertModel, num_classes: int):
        super().__init__()
        self.bert = bert
        self.classifier = nn.Linear(bert.config.hidden_size, num_classes)
        
    def forward(self, input_ids, attention_mask=None, token_type_ids=None, position_ids=None, head_mask=None,
                
            labels=None):
        outputs = self.bert(input_ids,
                               attention_mask=attention_mask,
                               token_type_ids=token_type_ids,
                               position_ids=position_ids,
                               head_mask=head_mask)
        cls_output = outputs[1] # batch, hidden
        cls_output = self.classifier(cls_output) # batch, 6
        cls_output = torch.sigmoid(cls_output)
        criterion = nn.BCELoss()
        loss = 0
        if labels is not None:
            loss = criterion(cls_output, labels)
        return loss, cls_output


def create_model():
    model = BertClassifier(BertModel.from_pretrained(BERT_MODEL_NAME), 6).to(DEVICE)
    return model


def train(model, iterator, optimizer, scheduler):
    model.train()
    total_loss = 0
    for x, y in tqdm(iterator):
        optimizer.zero_grad()
        mask = (x != 0).float()
        loss, _outputs = model(x, attention_mask=mask, labels=y)
        total_loss += loss.item()
        loss.backward()
        optimizer.step()
        scheduler.step()
    print(f"Train loss {total_loss / len(iterator)}")

def evaluate(model, iterator):
    model.eval()
    pred = []
    true = []
    with torch.no_grad():
        total_loss = 0
        for x, y in tqdm(iterator):
            mask = (x != 0).float()
            loss, _outputs = model(x, attention_mask=mask, labels=y)
            total_loss += loss
            true += y.cpu().numpy().tolist()
            pred += _outputs.cpu().numpy().tolist()
    true = np.array(true)
    pred = np.array(pred)
    for i, name in enumerate(LABEL_COLS):
        print(f"{name} roc_auc {roc_auc_score(true[:, i], pred[:, i])}")
    print(f"Evaluate loss {total_loss / len(iterator)}")



def setup_scheduler_and_optimizer(model, train_iterator):
    no_decay = ['bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
    {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay': 0.01},
    {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
    ]
    warmup_steps = 10 ** 3
    total_steps = len(train_iterator) * EPOCH_NUM - warmup_steps
    optimizer = AdamW(optimizer_grouped_parameters, lr=2e-5, eps=1e-8)
    scheduler = get_linear_schedule_with_warmup(optimizer, warmup_steps, total_steps)
    return scheduler, optimizer


def main():
    model = create_model()
    train_df, val_df = setup_training_data()
    dev_iterator, train_iterator = setup_pytorch_dataloader(train_df, val_df)
    df_test, _, df_submission = setup_test_data()
    scheduler, optimizer = setup_scheduler_and_optimizer(model, train_iterator)
    for i in range(EPOCH_NUM):
        print('=' * 50, f"EPOCH {i}", '=' * 50)
        train(model, train_iterator, optimizer, scheduler)
        evaluate(model, dev_iterator)
    for i in tqdm(range(len(df_test) // BATCH_SIZE + 1)):
        batch_df = df_test.iloc[i * BATCH_SIZE: (i + 1) * BATCH_SIZE]
        assert (batch_df["id"] == df_submission["id"][i * BATCH_SIZE: (i + 1) * BATCH_SIZE]).all(), f"Id mismatch"
        texts = []
        for text in batch_df["comment_text"].tolist():
            text = TOKENIZER.encode(text, add_special_tokens=True)
            if len(text) > 120:
                text = text[:119] + [TOKENIZER.sep_token_id]
            texts.append(torch.LongTensor(text))
        padded_tokens = pad_sequence(texts, batch_first=True, padding_value=TOKENIZER.pad_token_id).to(DEVICE)
        mask = (padded_tokens != TOKENIZER.pad_token_id).float().to(DEVICE)
        with torch.no_grad():
            _, outputs = model(padded_tokens, attention_mask=mask)
        outputs = outputs.cpu().numpy()
        df_submission.loc[(i * BATCH_SIZE):((i + 1) * BATCH_SIZE) - 1,LABEL_COLS] = outputs
    results_df = df_submission.merge(df_test, left_on="id", right_on="id")
    return results_df, model


def generate_single_predictions(model, input_text_to_analyze):

    input_text_tokens = TOKENIZER.encode(input_text_to_analyze, add_special_tokens=True)
    if len(input_text_tokens) > 120:
        input_text_tokens = input_text_tokens[:119] + [TOKENIZER.sep_token_id]
    texts_tokens_list = [torch.LongTensor(input_text_tokens)]
    padded_tokens = pad_sequence(texts_tokens_list, batch_first=True, padding_value=TOKENIZER.pad_token_id).to(DEVICE)
    text_mask = (padded_tokens != TOKENIZER.pad_token_id).float().to(DEVICE)
    with torch.no_grad():
        _, input_text_outputs = model(padded_tokens, attention_mask=text_mask)
    input_text_outputs = input_text_outputs.cpu().numpy()

    predictions_label = []
    preds = input_text_outputs[0]
    for idx, label in enumerate(LABEL_COLS):
        if preds[idx] > 0.5:
            predictions_label.append((label, round(preds[idx]*100, 2)))
    return predictions_label


def save_model(model, path_to_data_files=PATH_TO_DATA, model_files_path='pytorch_model/230821_model.pt'):
    torch.save(model.state_dict(), f'{path_to_data_files}/{model_files_path}')
    return True


def load_saved_model(path_to_data_files=PATH_TO_DATA, model_files_path='pytorch_model/230821_model.pt', cuda=True):
    saved_model = BertClassifier(BertModel.from_pretrained(BERT_MODEL_NAME), 6).to(DEVICE)
    if cuda:
        saved_model.load_state_dict(torch.load(f'{path_to_data_files}/{model_files_path}'))
    else:
        saved_model.load_state_dict(torch.load(f'{path_to_data_files}/{model_files_path}', map_location=torch.device('cpu')))
    return saved_model
